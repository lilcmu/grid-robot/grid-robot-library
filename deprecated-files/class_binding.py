# LEGO type:advanced slot:5 autostart

import runtime
import utime
import sys
import system
from mindstorms import MSHub


class RPC:
    def emit(self, op, id):
        pass


class BindingTest():
    def __init__(self):
        self.hub = MSHub()
        self.commands_list = []
        self.start_exec = False

    async def on_start(self, vm, stack):
        print("on start running ...")
        cmdIndex = 0
        while True:
            if self.commands_list and self.start_exec:
                print("exec :", self.commands_list[cmdIndex])
                stacks = vm.broadcast(self.commands_list[cmdIndex])

                while any(stack.is_active() for stack in stacks):
                    yield

                cmdIndex += 1
                if cmdIndex == len(self.commands_list):
                    print("end program")
                    cmdIndex = 0
                    vm.stop()

            yield

    async def on_display(self, vm, stack):
        await vm.system.display.write_async("Hello")

    async def on_beep(self, vm, stack):
        self.hub.speaker.beep(60, 0.4)
        self.hub.speaker.beep(72, 0.3)
        self.hub.speaker.beep(84, 0.2)

    def setup(self, rpc, system, stop):
        vm = runtime.VirtualMachine(rpc, system, stop, "binding_test")
        vm.register_on_start("on_start", self.on_start)
        vm.register_on_broadcast("on_display", self.on_display, "display")
        vm.register_on_broadcast("on_beep", self.on_beep, "beep")
        return vm

    def addCmd(self, cmd, isLastCmd=False):
        self.commands_list.append(cmd)
        print("cmd list: ", self.commands_list)
        if isLastCmd:
            self.start_exec = True


if __name__ is not None:
    print(__name__)

    test = BindingTest()

    # ? user commands from block(s) ...
    test.addCmd("beep")
    test.addCmd("display", True)
