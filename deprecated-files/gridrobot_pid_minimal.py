# LEGO type:standard slot:0

# ? ---------------------------------------------------
# ?
# ? Grid Robot library - A library for mindstorms robot inventor
# ?    this library wraps motor control using yaw angle from
# ?    built-in axis sensor. Also moving and keeping distance
# ?    by color markers on a grid track
# ?
# ? Written by Peeranut (Sun) Pongpakatien - peeranut32@gmail.com
# ?
# ? Thanks to all refs.
# ?    https://github.com/LEGO/MINDSTORMS-Robot-Inventor-hub-API
# ?    https://github.com/maarten-pennings/Lego-Mindstorms
# ?    https://github.com/PeterStaev/lego-spikeprime-mindstorms-vscode
# ?    https://github.com/azzieg/mindstorms-inventor
# ?
# ? -------------- Usage ------------------------------
# ?
# ?    from gridrobot import grid_robot
# ?
# ?    grid_robot.move_forward()
# ?    grid_robot.turn_left()
# ?    grid_robot.move_forward(2)
# ?    grid_robot.turn_right(2)
# ?
# ? ---------------------------------------------------

from mindstorms import MSHub, MotorPair, ColorSensor, DistanceSensor
import utime, math, sys

GRID_DISTANCE = const(8)

MOTOR_MOVE_SPEED = const(30)
MOTOR_TURN_SPEED = const(20)

DIR_LEFT = const(0)
DIR_RIGHT = const(1)

CONTROL_MOVE_KP = 0.7
CONTROL_MOVE_KI = 0.05
CONTROL_MOVE_MAX = MOTOR_MOVE_SPEED
CONTROL_MOVE_MIN = -MOTOR_MOVE_SPEED
CONTROL_ROTATE_KP = 0.1
CONTROL_ROTATE_ERROR_MARGIN = 3
CONTROL_ROTATE_MAX = MOTOR_TURN_SPEED
CONTROL_ROTATE_MIN = -MOTOR_TURN_SPEED

class GridRobot:
    def __init__(self):
        # ? robot I/O variables
        self.__lego_hub = MSHub()
        self.__motor_pair = MotorPair('A', 'B')
        self.__left_sensor = ColorSensor('C')
        self.__right_sensor = ColorSensor('D')
        self.__front_sensor = DistanceSensor('E')

        self.__motion_sensor = self.__lego_hub.motion_sensor
        self.__beeper = self.__lego_hub.speaker

        # ? robot awareness variables
        self.__prev_color = 0
        self.__curr_direction = 0

        # ? motor control variables
        self.__control_proportional = 0
        self.__control_integral = 0

    def __constrain(self, value, min_value, max_value):
        return min(max_value, max(min_value, value))

    def __normalize_direction(self, value):
        return ((value + 180) % 360) - 180

    def __reset_control_parameters(self):
        self.__control_proportional = 0
        self.__control_integral = 0

    def __beep(self, isBeep=True):
        if isBeep:
            self.__beeper.beep()
        else:
            self.__beeper.beep(60, 0.4)
            self.__beeper.beep(72, 0.3)
            self.__beeper.beep(84, 0.2)

    def __get_color_indicator(self):
        l_color = self.__left_sensor.get_color()
        r_color = self.__right_sensor.get_color()

        if l_color and r_color:
            if l_color == r_color:
                return l_color
        return None

    def __found_next_color_indicator(self):
        l_color = self.__left_sensor.get_color()
        r_color = self.__right_sensor.get_color()

        if l_color and r_color:
            if l_color == r_color:
                if l_color != 'white' and l_color != 'black':
                    if self.__prev_color != l_color:
                        self.__prev_color = l_color
                        return l_color
                else:
                    self.__prev_color = l_color
        return None 

    def __read_reflective_sensors(self):
        return self.__left_sensor.get_reflected_light(), self.__right_sensor.get_reflected_light()

    def __found_obstacle(self):
        # ? sampling 5 samples average
        sum_distance = 0
        for i in range(5):
            distance = self.__front_sensor.get_distance_cm()
            if distance is None:
                distance = 200
            sum_distance += distance
        if (sum_distance / 5) < (GRID_DISTANCE - 3):
            return True
        return False

    def __adjust_steering_movement(self, left_sensor, right_sensor):
        error =  right_sensor - left_sensor

        self.__control_proportional = CONTROL_MOVE_KP * error
        self.__control_integral += CONTROL_MOVE_KI * error

        # ? prevent control i windup .. constrain control i
        self.__control_integral = self.__constrain(self.__control_integral, CONTROL_MOVE_MIN, CONTROL_MOVE_MAX)

        output = self.__control_proportional + self.__control_integral

        return self.__constrain(output, CONTROL_MOVE_MIN, CONTROL_MOVE_MAX), error


    def __adjust_rotate_movement(self, target_angle):
        yaw = self.__motion_sensor.get_yaw_angle()

        # ? compute error with reversal over limit error (when yaw angle over 180 -> -180 of another side
        error = target_angle - yaw
        if error > 180: error = -(error - 180)
        if error < -180: error = -(error + 180)

        # ? calculate turn rate
        output = CONTROL_ROTATE_KP * error

        return self.__constrain(output, CONTROL_ROTATE_MIN, CONTROL_ROTATE_MAX), error

    def __blinking_wrong_action(self):
        for i in range(3):
            self.__lego_hub.status_light.on('red')
            utime.sleep(0.3)
            self.__lego_hub.status_light.off()
            utime.sleep(0.2)

    def __check_wrong_move(self):
        if self.__found_obstacle():
            self.__blinking_wrong_action()
            sys.exit(0)

    # ? there is no wrong for rotation ??
    def __check_wrong_rotate(self):
        pass

    def __move_forward(self):
        while not self.__found_obstacle() and not self.__found_next_color_indicator():
            l_sensor, r_sensor = self.__read_reflective_sensors()

            control_value, error = self.__adjust_steering_movement(l_sensor, r_sensor)

            left_motor = int(MOTOR_MOVE_SPEED + control_value)
            right_motor = int(MOTOR_MOVE_SPEED - control_value)
            self.__motor_pair.start_tank_at_power(left_motor, right_motor)
        self.__motor_pair.stop()
        self.__reset_control_parameters()

    def __turn_direction(self, direction):
        if direction == DIR_LEFT:
            self.__curr_direction -= 90
        elif direction == DIR_RIGHT:
            self.__curr_direction += 90

        control_value = 1  # ? dummy error
        error = 0
        target = self.__normalize_direction(self.__curr_direction)
        while math.fabs(error) > CONTROL_ROTATE_ERROR_MARGIN:
            control_value, error = self.__adjust_rotate_movement(target)

            control_power = int((-MOTOR_TURN_SPEED if control_value < 0 else MOTOR_TURN_SPEED) + control_value)
            self.__motor_pair.start_tank_at_power(control_power, -control_power)
        self.__motor_pair.stop()
        self.__reset_control_parameters()

    def start_robot(self):
        self.__beep(False)

        # ! when press left and right button on start it will calibrating motion sensor
        if self.__lego_hub.left_button.is_pressed() and self.__lego_hub.right_button.is_pressed():
            print("calibrate motion sensor ..")
            self.__motion_sensor.reset_yaw_angle()
            self.__beep()
            utime.sleep(1)

        # ? guessing current direction from yaw value
        front_dir = range(-45, 44)
        left_dir = range(-134, -46)
        right_dir = range(45, 134)
        curr_yaw = self.__motion_sensor.get_yaw_angle()
        if curr_yaw in front_dir:
            self.__curr_direction = 0
        elif curr_yaw in left_dir:
            self.__curr_direction = -90
        elif curr_yaw in right_dir:
            self.__curr_direction = 90
        else:
            self.__curr_direction = -180
        print("start direction:", self.__curr_direction)

        # ? getting current color marker
        self.__prev_color = self.__get_color_indicator()

    def move_forward(self, repeat_count=1):
        for _ in range(repeat_count):
            self.__check_wrong_move()
            self.__move_forward()
            self.__beep()

    def turn_left(self, repeat_count=1):
        for _ in range(repeat_count):
            self.__check_wrong_rotate()
            self.__turn_direction(DIR_LEFT)
            self.__beep()

    def turn_right(self, repeat_count=1):
        for _ in range(repeat_count):
            self.__check_wrong_rotate()
            self.__turn_direction(DIR_RIGHT)
            self.__beep()


# * pre-initialize class object
grid_robot = GridRobot()
grid_robot.start_robot()


if __name__ != None:
    print(__name__)
    print("starting Grid Robot !")
    grid_robot.move_forward()
    grid_robot.turn_left()
    grid_robot.move_forward(2)
    grid_robot.turn_left()
    grid_robot.move_forward()
    grid_robot.turn_right(2)
    grid_robot.move_forward(4)
    grid_robot.turn_left()
    grid_robot.move_forward(2)
    grid_robot.turn_right(1)
    grid_robot.move_forward()

