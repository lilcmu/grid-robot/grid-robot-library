# LEGO type:advanced slot:0

from runtime import VirtualMachine
from mindstorms import MSHub, MotorPair, ColorSensor, DistanceSensor
import math, utime

GRID_DISTANCE = const(9)
STEERING_YAW_TRUST_RESET = const(50)

MOTOR_DEFAULT_SPEED = const(30)
MOTOR_STEERING_MULTIPLER = const(-8)
MOTOR_SIDE_MULTIPLER = const(-10)
MOTOR_TURN_ANGLE = const(86)
MOTOR_SIDE_DISTANCE_STEERING = const(50)

ROBOT_STOP = const(0)
ROBOT_FORWARD = const(1)
ROBOT_BACKWARD = const(2)
ROBOT_LEFT = const(3)
ROBOT_RIGHT = const(4)

class GridRobot:
    def __init__(self):
        self.hub = MSHub()
        self.motor_pair = MotorPair('A', 'B')
        self.color_detector = ColorSensor('C')
        self.side_distance = DistanceSensor('D')
        self.front_distance = DistanceSensor('E')

        self.commands_list = []
        self.previous_movement = ROBOT_STOP
        self.previous_color = None
        self.yaw_offset = 0
        self.steering_offset = 0
        # self.steering_trust_factor = 0
        self.steering_side_adjustment = 0
        self.steering_yaw_adjustment = 0


    def startRobot(self):
        # setup motor default settings
        self.motor_pair.set_default_speed(MOTOR_DEFAULT_SPEED)

        # reset yaw value at start
        self.hub.motion_sensor.reset_yaw_angle()

        # initialize variables
        self.previous_color = self.color_detector.get_color()

        self.statusBeep(False)
        # self.loop()


    def statusBeep(self, isBeep=True):
        if isBeep:
            self.hub.speaker.beep()
        else:
            self.hub.speaker.beep(60, 0.4)
            self.hub.speaker.beep(72, 0.3)
            self.hub.speaker.beep(84, 0.2)


    def checkObstacle(self):
        distance = self.front_distance.get_distance_cm()
        if distance:
            if distance < (GRID_DISTANCE - 3):
                return True
        return False


    # def checkNextColorIndicator(self):
    #    color = self.color_detector.get_color()
    #    if state_has_changed("self.previous_color != color", self.previous_color != color):
    #        if color != None or color != 'white':
    #            self.previous_color = color
    #            return color
    #    return None


    def foundNextColorIndicator(self):
        color = self.color_detector.get_color()
        if color != None:
            if color != 'white':
                if self.previous_color != color:
                    print('found new color:', color)
                    self.previous_color = color
                    return True
            else:
                self.previous_color = color
        return False


    def checkWrongMovement(self):
        # cannot move to next block or wrong direction
        return


    def adjustSteeringMovement(self):
        # update yaw if previous robot can move straight by follows side detector
        # if self.steering_trust_factor >= STEERING_YAW_TRUST_RESET:
        #    self.hub.motion_sensor.reset_yaw_angle()
        #    self.steering_trust_factor = 0
        #    print('yaw reset ..')

        current_yaw = self.hub.motion_sensor.get_yaw_angle()
        current_yaw -= self.yaw_offset
        if current_yaw != 0:
            self.steering_yaw_adjustment = current_yaw * MOTOR_STEERING_MULTIPLER
            # print('yaw adjust:', self.steering_yaw_adjustment)
            # self.steering_offset = math.floor(self.steering_yaw_adjustment * 0.5)


    def adjustSideDistance(self):
        distance = self.side_distance.get_distance_cm()
        if distance:
            diff_distance = GRID_DISTANCE - distance
            # checking need to adjust distance ? and check if delta too much maybe error or crossing junction, so no need to adjust
            if diff_distance != 0 and abs(diff_distance) < GRID_DISTANCE:
                self.steering_side_adjustment = diff_distance * MOTOR_SIDE_MULTIPLER
                # self.steering_side_adjustment = diff_distance * MOTOR_SIDE_MULTIPLER
                # print('side adjust:', self.steering_side_adjustment)
                # self.motor_pair.start(self.steering_side_adjustment)


    def moveForward(self):
        # before move must be check color along with current robot direction
        self.checkWrongMovement()

        while not self.checkObstacle() and not self.foundNextColorIndicator():
            self.motor_pair.start(self.steering_offset)
            self.adjustSteeringMovement()
            self.adjustSideDistance()
            self.steering_offset = math.floor((self.steering_yaw_adjustment * 0.5) + (self.steering_side_adjustment * 0.5))
            print('steer:', self.steering_offset)
        self.motor_pair.stop()

        self.previous_movement = ROBOT_FORWARD


    def moveBackward(self):
        return


    def turnLeft(self):
        # getting current yaw to offset some previous error angle
        # current_yaw = self.hub.motion_sensor.get_yaw_angle()
        # turnAngle = current_yaw + MOTOR_MOVE_ANGLE - self.yaw_offset
        # # turnAngle = MOTOR_MOVE_ANGLE
        # print('turn:', turnAngle)
        # self.motor_pair.move(turnAngle, 'degrees', -100)

        targetYaw = self.yaw_offset - MOTOR_TURN_ANGLE
        while self.hub.motion_sensor.get_yaw_angle() > targetYaw:
            self.motor_pair.start(-100, 10)
        self.motor_pair.stop()

        # reset yaw angle and update previous activity
        # self.hub.motion_sensor.reset_yaw_angle()
        self.yaw_offset = self.hub.motion_sensor.get_yaw_angle()
        previous_movement = ROBOT_LEFT


    def turnRight(self):
        # getting current yaw to offset some previous error angle
        # current_yaw = self.hub.motion_sensor.get_yaw_angle()
        # turnAngle = current_yaw + MOTOR_MOVE_ANGLE - self.yaw_offset
        # # turnAngle = MOTOR_MOVE_ANGLE
        # print('turn:', turnAngle)
        # self.motor_pair.move(turnAngle, 'degrees', 100)

        targetYaw = self.yaw_offset + MOTOR_TURN_ANGLE
        while self.hub.motion_sensor.get_yaw_angle() < targetYaw:
            self.motor_pair.start(100, 10)
        self.motor_pair.stop()

        # reset yaw angle and update previous activity
        # self.hub.motion_sensor.reset_yaw_angle()
        self.yaw_offset = self.hub.motion_sensor.get_yaw_angle()
        previous_movement = ROBOT_RIGHT


    def execCommands(self):
        # getting commands from user program file
        # try:
        #    grid_file = open('./grid_files/grid.txt', 'r')
        # except OSError:
        #    print('file not found')
        #    return

        # self.commands_list = grid_file.read().splitlines()
        # grid_file.close()

        print('commands:', self.commands_list)

        # handles user commands execution and user events
        if self.commands_list:
            for command in self.commands_list:
                print('exec:', command)
                # handles grid adjustment and robot movement
                if command == 'forward':
                    self.moveForward()
                elif command == 'backward':
                    self.moveBackward()
                elif command == 'left':
                    self.turnLeft()
                elif command == 'right':
                    self.turnRight()
                else:
                    print('command not found')
                utime.sleep_ms(500)
            self.statusBeep(False)
        else:
            print('command list empty')



    def loop(self):
        # while True:
        #    if self.hub.left_button.is_pressed() or self.hub.right_button.is_pressed():
        #        self.statusBeep()

        self.execCommands()


    async def on_start(vm, stack):
        vm.broadcast("exec")


def setup(rpc, system, stop):
    
    vm = VirtualMachine(rpc, system, stop, "grid_robot")
    vm.register_on_start("grid->onstart", on_start)
    vm.register_on_broadcast("grid->onexec", on_exec, "exec")

if __name__ != None:
    print(__name__)
    print("starting Grid Robot !")
    
    commands_list = ['forward', 'left', 'forward', 'forward', 'right', 'forward', 'left', 'forward']
    robot = GridRobot()
    robot.startRobot()
