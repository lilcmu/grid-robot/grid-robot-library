# LEGO type:standard slot:0

# ? ---------------------------------------------------
# ?
# ? Grid Robot library - A library for mindstorms robot inventor
# ?    this library wraps motor control using yaw angle from
# ?    built-in axis sensor. Also moving and keeping distance
# ?    by color markers on a grid track
# ?
# ? Written by Peeranut (Sun) Pongpakatien - peeranut32@gmail.com
# ?
# ? Thanks to all refs.
# ?    https://github.com/LEGO/MINDSTORMS-Robot-Inventor-hub-API
# ?    https://github.com/maarten-pennings/Lego-Mindstorms
# ?    https://github.com/PeterStaev/lego-spikeprime-mindstorms-vscode
# ?    https://github.com/azzieg/mindstorms-inventor
# ?
# ? -------------- Usage ------------------------------
# ?
# ?    from gridrobot import grid_robot
# ?
# ?    grid_robot.move_forward()
# ?    grid_robot.turn_left()
# ?    grid_robot.move_forward(2)
# ?    grid_robot.turn_right(2)
# ?
# ? ---------------------------------------------------

from mindstorms import MSHub, MotorPair, ColorSensor, DistanceSensor
import utime

GRID_DISTANCE = const(8)

MOTOR_MOVE_SPEED = const(30)
MOTOR_TURN_SPEED = const(20)

DIR_LEFT = const(0)
DIR_RIGHT = const(1)

CONTROL_INTERVAL_MS = 10
CONTROL_INTERVAL_SEC = CONTROL_INTERVAL_MS / 1000
CONTROL_MOVE_KP = 0.7
CONTROL_MOVE_KI = 0.1 * CONTROL_INTERVAL_SEC
CONTROL_MOVE_KD = 0
CONTROL_MOVE_MAX = MOTOR_MOVE_SPEED
CONTROL_MOVE_MIN = -MOTOR_MOVE_SPEED
CONTROL_ROTATE_KP = 0.2
CONTROL_ROTATE_KI = 0
CONTROL_ROTATE_KD = 0.005 / CONTROL_INTERVAL_SEC
CONTROL_ROTATE_ERROR_MARGIN = 0.4
CONTROL_ROTATE_MAX = MOTOR_TURN_SPEED
CONTROL_ROTATE_MIN = -MOTOR_TURN_SPEED


class GridRobot:
    def __init__(self):
        # ? robot I/O variables
        self.__lego_hub = MSHub()
        self.__motor_pair = MotorPair('A', 'B')
        self.__color_sensor = ColorSensor('C')
        self.__side_sensor = DistanceSensor('D')
        self.__front_sensor = DistanceSensor('E')

        self.__motion_sensor = self.__lego_hub.motion_sensor
        self.__beeper = self.__lego_hub.speaker

        # ? robot awareness variables
        self.__prev_color = 0
        self.__curr_direction = 0

        # ? motor control variables
        self.__control_proportional = 0
        self.__control_integral = 0
        self.__control_derivative = 0
        self.__control_prev_input = 0
        self.__control_prev_output = 0
        self.__control_last_update = 0

    def __constrain(self, value, min_value, max_value):
        return min(max_value, max(min_value, value))

    def __normalize_direction(self, value):
        return ((value + 180) % 360) - 180

    def __calculate_pid_control(self, input, set_point, out_min, out_max, Kp=0, Ki=0, Kd=0):
        output = self.__control_prev_output
        curr_update_time = utime.ticks_ms()
        if utime.ticks_diff(curr_update_time, self.__control_last_update) > CONTROL_INTERVAL_MS:
            # ? compute error with reversal over limit error (when yaw angle over 180 -> -180 of another side
            error = set_point - input
            if error > 180:
                error = -(error - 180)
            if error < -180:
                error = -(error + 180)

            # ? compute PID variables
            self.__control_proportional = Kp * error
            self.__control_integral += Ki * error
            self.__control_derivative = Kd * \
                (input - self.__control_prev_input)

            # ? prevent control i windup .. constrain control i
            self.__control_integral = self.__constrain(
                self.__control_integral, out_min, out_max)

            # ? compute output control
            output = self.__constrain((self.__control_proportional + self.__control_integral -
                                       self.__control_derivative), CONTROL_MOVE_MIN, CONTROL_MOVE_MAX)

            # ? update variables for next compute
            self.__control_prev_input = input
            self.__control_prev_output = output
            self.__control_last_update = curr_update_time
        return output

    def __reset_control_parameters(self):
        self.__control_proportional = 0
        self.__control_integral = 0
        self.__control_derivative = 0
        self.__control_prev_input = 0
        self.__control_prev_output = 0
        self.__control_last_update = 0

    def __beep(self, isBeep=True):
        if isBeep:
            self.__beeper.beep()
        else:
            self.__beeper.beep(60, 0.4)
            self.__beeper.beep(72, 0.3)
            self.__beeper.beep(84, 0.2)

    def __found_next_color_indicator(self):
        color = self.__color_sensor.get_color()
        if color != None:
            if color != 'white':
                if self.__prev_color != color:
                    self.__prev_color = color
                    return color
            else:
                self.__prev_color = color
        return False

    def __found_obstacle(self):
        # ? sampling 5 samples average
        sum_distance = 0
        for i in range(5):
            distance = self.__front_sensor.get_distance_cm()
            if distance is None:
                distance = 200
            sum_distance += distance
        if (sum_distance / 5) < (GRID_DISTANCE - 3):
            return True
        return False

    # * tracking yaw value with PI-control, return control value
    def __adjust_steering_movement(self, target_direction):
        yaw = self.__motion_sensor.get_yaw_angle()
        return self.__calculate_pid_control(yaw, target_direction, CONTROL_MOVE_MIN, CONTROL_MOVE_MAX, Kp=CONTROL_MOVE_KP, Ki=CONTROL_MOVE_KI, Kd=CONTROL_MOVE_KD)

    # * tracking yaw value while rotate with PD-control, return control value
    def __adjust_rotate_movement(self, target_direction):
        yaw = self.__motion_sensor.get_yaw_angle()
        return self.__calculate_pid_control(yaw, target_direction, CONTROL_ROTATE_MIN, CONTROL_ROTATE_MAX, Kp=CONTROL_ROTATE_KP, Ki=CONTROL_ROTATE_KI, Kd=CONTROL_ROTATE_KD)

    # * tracking side distance, return side control adjustment (0 to MOTOR_DEFAULT_SPEED)
    def __adjust_side_distance(self):
        diff_distance = 0
        distance = self.__side_sensor.get_distance_cm()
        # ? if distance out of acceptable range skip current adjust
        if distance:
            diff_distance = distance - GRID_DISTANCE
            if abs(diff_distance) > GRID_DISTANCE:
                diff_distance = 0
        return self.__constrain(diff_distance * 3, CONTROL_MOVE_MIN, CONTROL_MOVE_MAX)

    def __blinking_wrong_action(self):
        for i in range(3):
            self.__lego_hub.status_light.on('red')
            utime.sleep(0.3)
            self.__lego_hub.status_light.off()
            utime.sleep(0.2)

    def __check_wrong_move(self):
        pass

    def __check_wrong_rotate(self):
        pass

    # todo: moving forward by keeping track on yaw value and side distance sensor
    def __move_forward(self):
        # ? normalize target direction (yaw value)
        target = self.__normalize_direction(self.__curr_direction)
        print("target dir:", target)
        color_count = 0
        start_brake = False
        while not self.__found_obstacle() and not color_count >= 2:
            # while not self.__found_obstacle() and not self.__found_next_color_indicator():
            if self.__found_next_color_indicator():
                start_brake = True
            if start_brake:
                color_count += 1

            # ? getting adjustment values
            control_value = self.__adjust_steering_movement(
                target) + self.__adjust_side_distance()
            # control_value = self.__adjust_steering_movement(target)
            # print("move PI: ", control_value)
            # control_dist = self.__adjust_side_distance()
            # print("move Dist: ", control_dist)
            # control_value += control_dist
            print("move:", control_value)

            left_motor = int(MOTOR_MOVE_SPEED + control_value)
            right_motor = int(MOTOR_MOVE_SPEED - control_value)
            self.__motor_pair.start_tank_at_power(left_motor, right_motor)
        self.__motor_pair.stop()
        self.__reset_control_parameters()

    # todo: turn left or right by yaw value with PD-control
    def __turn_direction(self, direction):
        if direction == DIR_LEFT:
            self.__curr_direction -= 90
        elif direction == DIR_RIGHT:
            self.__curr_direction += 90

        control_value = 1  # ? dummy error
        target = self.__normalize_direction(self.__curr_direction)
        print("target dir:", target)
        while abs(control_value) > CONTROL_ROTATE_ERROR_MARGIN:
            control_value = self.__adjust_rotate_movement(target)
            print("turn: ", control_value)

            control_power = int(
                (-MOTOR_TURN_SPEED if control_value < 0 else MOTOR_TURN_SPEED) + control_value)
            self.__motor_pair.start_tank_at_power(
                control_power, -control_power)
        self.__motor_pair.stop()
        self.__reset_control_parameters()

    def start_robot(self):
        self.__beep(False)

        # ! when press left and right button on start it will calibrating motion sensor
        if self.__lego_hub.left_button.is_pressed() and self.__lego_hub.right_button.is_pressed():
            print("calibrate motion sensor ..")
            self.__motion_sensor.reset_yaw_angle()
            self.__beep()
            utime.sleep(1)

        # ? guessing current direction from yaw value
        front_dir = range(-45, 44)
        left_dir = range(-134, -46)
        right_dir = range(45, 134)
        curr_yaw = self.__motion_sensor.get_yaw_angle()
        if curr_yaw in front_dir:
            self.__curr_direction = 0
        elif curr_yaw in left_dir:
            self.__curr_direction = -90
        elif curr_yaw in right_dir:
            self.__curr_direction = 90
        else:
            self.__curr_direction = -180
        print("start direction:", self.__curr_direction)

        # ? getting current color marker
        self.__prev_color = self.__color_sensor.get_color()

    def move_forward(self, repeat_count=1):
        for i in range(repeat_count):
            self.__check_wrong_move()
            self.__move_forward()
            self.__beep()

    def turn_left(self, repeat_count=1):
        for i in range(repeat_count):
            self.__check_wrong_rotate()
            self.__turn_direction(DIR_LEFT)
            self.__beep()

    def turn_right(self, repeat_count=1):
        for i in range(repeat_count):
            self.__check_wrong_rotate()
            self.__turn_direction(DIR_RIGHT)
            self.__beep()


# * pre-initialize class object
grid_robot = GridRobot()
grid_robot.start_robot()


if __name__ != None:
    print(__name__)
    print("starting Grid Robot !")
    grid_robot.move_forward()
    grid_robot.turn_left()
    grid_robot.move_forward(2)
    grid_robot.turn_left()
    grid_robot.move_forward()
    grid_robot.turn_right(2)
    grid_robot.move_forward(4)
    grid_robot.turn_left()
    grid_robot.move_forward(2)
    grid_robot.turn_right(1)
    grid_robot.move_forward()
