# LEGO type:standard slot:0

# ? ---------------------------------------------------
# ?
# ? Grid Robot library - A library for mindstorms robot inventor
# ?    this library wraps motor control using yaw angle from
# ?    built-in axis sensor. Also moving and keeping distance
# ?    by color markers on a grid track
# ?
# ? Written by Peeranut (Sun) Pongpakatien - peeranut32@gmail.com
# ?
# ? Thanks to all refs.
# ?    https://github.com/LEGO/MINDSTORMS-Robot-Inventor-hub-API
# ?    https://github.com/maarten-pennings/Lego-Mindstorms
# ?    https://github.com/azzieg/mindstorms-inventor
# ?
# ? -------------- Usage ------------------------------
# ?
# ?    from gridrobot import grid_robot
# ?
# ?    grid_robot.move_forward()
# ?    grid_robot.turn_left(
# ?    grid_robot.move_forward(2)
# ?    grid_robot.turn_right(2)
# ?
# ? ---------------------------------------------------

from mindstorms import MSHub, MotorPair, ColorSensor, DistanceSensor
# import utime, math, sysinfo: uncomment if needed
import utime

GRID_DISTANCE = const(10)
# LINE_BLACK = const(30)# warn: this used reflected value
LINE_WHITE = const(60)# warn: reading red not reflected, this must be scaled value (divided by 10)
LINE_YELLOW = const(90) # warn: reading red not reflected, this must be scaled value (divided by 10)
# LINE_THRESHOLD = (LINE_WHITE + LINE_BLACK) / 2
LINE_THRESHOLD = (LINE_WHITE + LINE_YELLOW) / 2

MOTOR_MOVE_SPEED = const(32)
MOTOR_ROTATE_SPEED = const(25)

DIR_FORWARD = const(0)
DIR_LEFT = const(1)
DIR_RIGHT = const(2)

CONTROL_MOVE_KP = 2.5
CONTROL_MOVE_MAX = MOTOR_MOVE_SPEED
CONTROL_MOVE_MIN = -MOTOR_MOVE_SPEED

CONTROL_ROTATE_KP = 0.3
CONTROL_ROTATE_MAX = MOTOR_ROTATE_SPEED
CONTROL_ROTATE_MIN = -MOTOR_ROTATE_SPEED
CONTROL_ROTATE_ERROR_MARGIN = 5
CONTROL_ROTATE_ANGLES = 90

LOOKING_LINE_LIMIT = 8
LOOKING_LINE_STEP = 0.4
LOOKING_LINE_RANGE = 5
LOOKING_LINE_RANGE_SCALE = 4

class GridRobot:
    def __init__(self):
        # INFO: robot I/O variables
        self.__lego_hub = MSHub()
        self.__motor_pair = MotorPair('A', 'B')
        self.__line_sensor = ColorSensor('C')
        self.__front_sensor = DistanceSensor('D')

        self.__motion_sensor = self.__lego_hub.motion_sensor
        self.__beeper = self.__lego_hub.speaker

        # ? robot awareness variables
        self.__prev_color = 0
        self.__curr_direction = 0
        self.__prev_rotation = DIR_LEFT

    def __constrain(self, value, min_value, max_value):
        return int(min(max_value, max(min_value, value)))

    def __scaled_red_intensity(self):
        rgb = self.__line_sensor.get_rgb_intensity()
        if rgb:
            return int(rgb[0] / 10)
        return LINE_THRESHOLD# NOTE: this will helps when calculating error on steering if it got no value
        # return int(self.__line_sensor.get_red() / 10)

    def __normalize_direction(self, value):
       return ((value + 180) % 360) - 180

    def __reversal_direction(self, angle):
        reversal_angle = angle
        if angle > 180: reversal_angle = -(angle - 180)
        if angle < -180: reversal_angle = -(angle + 180)
        return reversal_angle

    def __reset_robot_direction(self):
        self.__motion_sensor.reset_yaw_angle()
        self.__curr_direction = 0

    def __beep(self, isBeep=True):
        if isBeep:
            self.__beeper.beep()
        else:
            self.__beeper.beep(60, 0.4)
            self.__beeper.beep(72, 0.3)
            self.__beeper.beep(84, 0.2)

    def __found_next_color_indicator(self):
        l_color = self.__line_sensor.get_color()
        if l_color:
            if l_color != 'white' and l_color != 'yellow':
                if self.__prev_color != l_color:
                    self.__prev_color = l_color
                    return l_color
            else:
                self.__prev_color = l_color
        return None

    def __found_new_color_indicator(self):
        n_color = self.__line_sensor.get_color()
        if n_color:
            if n_color != self.__prev_color:
                return True
        return False

    def __found_line(self):
        # if self.__line_sensor.get_reflected_light() < LINE_THRESHOLD:
        sensor = self.__scaled_red_intensity()
        if sensor >= LINE_YELLOW - 4: # INFO: threshold at 90 - 4 -> 86
            return True
        return False

    def __found_obstacle(self):
        # ? sampling 5 samples average
        sum_distance = 0
        for _ in range(5):
            distance = self.__front_sensor.get_distance_cm()
            if distance is None:
                distance = 200
            sum_distance += distance
        if (sum_distance / 5) < (GRID_DISTANCE - 2):
            return True
        return False

    def __looking_for_line(self):
        if not self.__found_line():
            # * -- try to adjust by prev rotation at first
            steering = 100
            if self.__prev_rotation == DIR_RIGHT:
                steering = -steering

            # * -- extends search range, if still not found the line. limit on `LOOKING_LINE_LIMIT` extended times
            state = 0
            limit_found_line = LOOKING_LINE_LIMIT
            not_found = True
            while limit_found_line:
                # ? -- move forward a bit
                if state == 0:
                    self.__motor_pair.move(1, 'cm', 0, MOTOR_MOVE_SPEED - 5)
                    if (self.__found_line()):
                        limit_found_line = 0
                        not_found = False
                # ? -- looking on one side
                elif state == 1:
                    search_range = LOOKING_LINE_RANGE + (LOOKING_LINE_RANGE_SCALE * (LOOKING_LINE_LIMIT - limit_found_line))
                    for _ in range(search_range):
                        self.__motor_pair.move(LOOKING_LINE_STEP, 'cm', -steering, MOTOR_ROTATE_SPEED)
                        if (self.__found_line()):
                            limit_found_line = 0
                            not_found = False
                            break
                    if not_found:
                        self.__motor_pair.move(search_range * LOOKING_LINE_STEP, 'cm', steering, MOTOR_ROTATE_SPEED)
                # ? -- looking on other side
                elif state == 2:
                    search_range = LOOKING_LINE_RANGE + (LOOKING_LINE_RANGE_SCALE * (LOOKING_LINE_LIMIT - limit_found_line))
                    for _ in range(search_range):
                        self.__motor_pair.move(LOOKING_LINE_STEP, 'cm', steering, MOTOR_ROTATE_SPEED)
                        if (self.__found_line()):
                            limit_found_line = 0
                            not_found = False
                            break
                    if not_found:
                        self.__motor_pair.move(search_range * LOOKING_LINE_STEP, 'cm', -steering, MOTOR_ROTATE_SPEED)
                        limit_found_line -= 1 # ? update limit after have been looking on both side
                state += 1
                if state > 2:
                    state = 0

            if not_found:
                self.__blinking_wrong_action()

    def __adjust_steering_movement(self):
        # ? -- calculate from middle between white and black line
        # error = LINE_THRESHOLD - self.__line_sensor.get_reflected_light()
        # error = LINE_THRESHOLD - self.__scaled_red_intensity()
        sensor_value = self.__constrain(self.__scaled_red_intensity(), LINE_WHITE, LINE_YELLOW)
        error = sensor_value - LINE_THRESHOLD
        # ? -- if we have turn right, line checking will be inverted
        if self.__prev_rotation == DIR_RIGHT:
            error = -error
        output = CONTROL_MOVE_KP * error

        return int(output), error

    def __adjust_rotate_movement(self, target_angle):
        yaw = self.__motion_sensor.get_yaw_angle()

        # ? compute error with reversal over limit error (when yaw angle over 180 -> -180 of another side
        error = target_angle - yaw
        error = self.__reversal_direction(error)

        # ? calculate turn rate
        output = CONTROL_ROTATE_KP * error

        return self.__constrain(output, CONTROL_ROTATE_MIN, CONTROL_ROTATE_MAX), error

    def __blinking_wrong_action(self):
        for _ in range(3):
            self.__lego_hub.status_light.on('red')
            utime.sleep(0.3)
            self.__lego_hub.status_light.off()
            utime.sleep(0.2)
        raise SystemExit

    def __check_wrong_move(self):
        if self.__found_obstacle():
            self.__blinking_wrong_action()

    def __move_forward(self):
        # print('--- move forward ---')
        found_obstacle = False
        # * -- keep looking for line until found line to follow
        self.__looking_for_line()
        # print('found line')

        # * -- start line tracking
        # prev_control = 0
        while not self.__found_next_color_indicator():
            if self.__found_obstacle():
                found_obstacle = True
                break

            control_value, error = self.__adjust_steering_movement()
            # print('move control:', control_value, "err:", error)

            # NOTE: checking delta control not to growing up too rapidly
            # delta_control = abs(prev_control - control_value)
            # if delta_control > 30:
            #    control_value = int(delta_control / 2) + prev_control
            #    print('limit control:', control_value)
            # prev_control = control_value

            # info: -- keep checking does it on control (error not growing up)
            # info: -- maybe its not follows the line, just stop and move backward until got the line to track again
            # info: -- error will calculates together with delta direction between yaw value and current direction to keep adjusting
            # delta_dir = self.__curr_direction - self.__motion_sensor.get_yaw_angle() #? delta direction
            # delta_dir = self.__reversal_direction(delta_dir)
            # print('delta err:', delta_dir)

            # todo: -- if error keeps growing after 5 times adjustment, going back and try to reverses error calculation

            # note: -- runs the motors pair with control adjustment
            self.__motor_pair.start_at_power(MOTOR_MOVE_SPEED, control_value)
        # print('found next indicator')

        # * -- keep move until reach color marker edge
        while not self.__found_new_color_indicator() and not found_obstacle:
            self.__motor_pair.start_at_power(MOTOR_MOVE_SPEED, 0)
        # print('reach indicator edge')

        # * -- add further motors stopping delay
        utime.sleep_ms(200)
        self.__motor_pair.stop()
        self.__reset_robot_direction() # * -- assume that after moving forward, its a correct robot direction

    def __turn_direction(self, direction, last_rotation):
        # print('--- rotate direction ---')
        if direction == DIR_LEFT:
            self.__curr_direction -= CONTROL_ROTATE_ANGLES
        elif direction == DIR_RIGHT:
            self.__curr_direction += CONTROL_ROTATE_ANGLES

        target = self.__normalize_direction(self.__curr_direction)
        # print('rotate target:', target)
        # target = self.__curr_direction
        keepRotate = True
        while keepRotate:
            control_value, error = self.__adjust_rotate_movement(target)
            # print('rotate control:', control_value, "err:", error)

            # * -- rotate with one wheel
            # left_power = MOTOR_ROTATE_SPEED + control_value
            # right_power = MOTOR_ROTATE_SPEED - control_value
            # if control_value <= 0:
            #    left_power = math.copysign(left_power, control_value)
            # else:
            #    right_power = math.copysign(right_power, control_value)
            # self.__motor_pair.start_tank_at_power(int(left_power), int(right_power))
            # print(left_power, ",", right_power)

            # * -- rotate with both wheels
            control_power = int((-MOTOR_ROTATE_SPEED if control_value < 0 else MOTOR_ROTATE_SPEED) + control_value)
            self.__motor_pair.start_tank_at_power(control_power, -control_power)

            if last_rotation:
                # ? -- making sure we've rotated before cut off by line detection
                if (abs(error) < 60 and self.__found_line()) or abs(error) < CONTROL_ROTATE_ERROR_MARGIN:
                    keepRotate = False
                    # print('stop rotate with line')
            else:
                if abs(error) < CONTROL_ROTATE_ERROR_MARGIN:
                    keepRotate = False
                    # print('stop rotate with error')

        self.__motor_pair.stop()
        self.__prev_rotation = direction

    def start_robot(self):
        self.__beep(False)

        # ! -- always reset direction every time it's starting
        self.__reset_robot_direction()

        # ? -- getting current color marker
        self.__prev_color = self.__line_sensor.get_color()

    def move_forward(self, repeat_count=1):
        for _ in range(repeat_count):
            self.__check_wrong_move()
            self.__move_forward()
            self.__beep()

    def turn_left(self, repeat_count=1):
        last_rotate = False
        for ite in range(repeat_count):
            if repeat_count - ite <= 1:
                last_rotate = True
            self.__turn_direction(DIR_LEFT, last_rotate)
            self.__beep()

    def turn_right(self, repeat_count=1):
        last_rotate = False
        for ite in range(repeat_count):
            if repeat_count - ite <= 1:
                last_rotate = True
            self.__turn_direction(DIR_RIGHT, last_rotate)
            self.__beep()


# INFO: pre-initialize class object
grid_robot = GridRobot()
grid_robot.start_robot()

# grid_robot.move_forward()
# grid_robot.turn_left(3)
# grid_robot.move_forward()
# grid_robot.turn_right()
# grid_robot.move_forward()
# grid_robot.turn_left(2)


