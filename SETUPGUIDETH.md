# วิธีการติดตั้ง Grid Robot Library สำหรับ Mindstorms Robot Inventor

## สิ่งที่ต้องเตรียม
1. ติดตั้ง Python ขั้นต่ำเวอร์ชั่น 3.x สามารถติดตั้งได้จากลิ้งค์นี้ [python](https://www.python.org/downloads/)
2. ติดตั้ง Python modules หลังจากทำการติดตั้ง Python เรียบร้อยแล้ว โดยรันคำสั่งต่อไปนี้ใน terminal หรือ cmd
``` bash
python3 -m pip install pyserial tqdm
```
3. ดาวน์โหลดไฟล์ library และสคริปช่วยติดตั้งได้จากลิ้งค์นี้ [Grid Robot Library](https://gitlab.com/lilcmu/grid-robot/grid-robot-library/-/releases)
โดยให้ทำการดาวน์โหลดไฟล์ทั้งสอง(gridrobot.py และ cp.py) มาเก็บไว้ที่ Desktop

## ทำการติดตั้ง Library ลงตัวหุ่นยนต์
1. ทำการเชื่อมต่อหุ่นยนต์เข้ากับเครื่องคอมพิวเตอร์ผ่านสาย USB ทำการเช็คให้แน่ใจว่าไม่ได้เปิดโปรแกรม MINDSTORMS ทิ้งไว้
2. ทำการหา COM Port ที่เชื่อมต่ออยู่
2.1 ถ้าเป็น Windows ให้ทำการเปิด Device Manager แล้วดูที่ Ports
2.2 ถ้าเป็น MacOS ให้ทำการรันคำสั่งต่อไปนี้ใน terminal `ls -l /dev/tty.*` ปกติจะเป็นชื่อ tty.usbserial-xxxx
3. เมื่อทราบ port ของ Mindstorms เรียบร้อยแล้ว ให้เปิด terminal หรือ cmd ขึ้นมา แล้วเข้าไปที่โฟลเดอร์ที่เราดาวน์โหลดไฟล์มาเก็บไว้ ในที่นี้คือ Desktop โดยรันคำสั่ง `cd Desktop`
4. ทำการรันสคริปช่วยติดตั้งดังนี้
``` bash
python3 cp.py -t <COM Port ที่เราเช็คมา> gridrobot.py

# ตัวอย่าง Windows
python3 cp.py -t COM3 gridrobot.py

# ตัวอย่าง MacOS
python3 cp.py -t tty.usbserial-0001 gridrobot.py
```

หมายเหตุ ปกติแล้ว Mindstorms จะมี port อยู่ 2 port ด้วยกัน หากลองแล้วไม่ได้ให้ทำการเปลี่ยนไปลองอีก port นึงแทน

