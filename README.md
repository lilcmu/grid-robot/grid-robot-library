# Grid Robot Library: A MPY library for LEGO MindStorms Robot Inventor 51515
This library wraps motor control by using yaw value from built-in motion sensor. Also moving and tracking color markers on a grid.

## Usage
``` python
from gridrobot import grid_robot

grid_robot.move_forward()
grid_robot.turn_left()
grid_robot.move_forward(2)
grid_robot.turn_right(2)
```

## Setup the library
Thai setup guide can be found [Here](SETUPGUIDETH.md)

### Prerequisites
- Python - minimum version 3.x
- Python modules - `pyserial` and `tqdm`
  - Installs by running : `pip3 install pyserial tqdm`

### Installation
We need to put a library file(`gridrobot.py`) at root directory of MindStorms Robot by running copy script(`cp.py`)
``` bash
python3 mindstorms-tools/cp.py -t <your-lego-connected-port> gridrobot.py
```
- *`<your-lego-connected-port>`* - connects your MindStorms Robot with USB cable
  - **Windows** : Can be found in `device manager`. Normally named `COM` follows by number. eg. `COM5`
  - **MacOS** : Can be found by running `ls /dev/tty.*`. Normally named `tty.usbserial-` follows by number. eg. `/dev/tty.usbserial-0923987`

## Resources
- [`spike-tools`](https://github.com/nutki/spike-tools) for copy script to sending library file to MindStorms Robot
- [`dfu-pybricks`](https://dfu.pybricks.com/) in case of LEGO stuck in update loop, using this tool will reset all the flash
- Others documentation: [Official LEGO Docs](https://github.com/LEGO/MINDSTORMS-Robot-Inventor-hub-API), [maarten-pennings's docs](https://github.com/maarten-pennings/Lego-Mindstorms) and [azzieg's docs](https://github.com/azzieg/mindstorms-inventor)
